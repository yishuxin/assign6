package problem2;

public class Coin implements Lockable
{
   private final int HEADS = 0;
   private final int TAILS = 1;
   private int face, key;
   private boolean locked;
   
   //check if locked
   public boolean locked()
   {
	   return this.locked;
   }
   
   //set password
   public void setKey(int password)
   {
	   this.key = password;	   
   }
   //get password
   public int getKey()
   {
	  return key;	   
   }
   
   //lock coin
   public void lock(int key)
   {
	   if (key == this.key)
		   locked = true;			   
   }
   
   //unlock coin
   public void unlock(int key)
   {
	   if (key == this.key)
		   locked = false;
   }
   
   //-----------------------------------------------------------------
   //  Sets up the coin by flipping it initially.
   //-----------------------------------------------------------------
   public Coin()
{
flip();
}
   //-----------------------------------------------------------------
   //  Flips the coin by randomly choosing a face value.
   //-----------------------------------------------------------------
   public void flip()
   {  
	  if(locked) {
		  System.out.println("cannot flip: locked");
		        return;
        }else
      face = (int) (Math.random() * 2);
}
   //-----------------------------------------------------------------
   //  Returns true if the current face of the coin is heads.
   //-----------------------------------------------------------------
   public boolean isHeads()
   { if (locked) {
	      System.out.println("cannot determine value: locked");
          return false;  // should throw an exception
           }else

      return (face == HEADS);
}
   //-----------------------------------------------------------------
   // Returns the current face of the coin as a string.
   //-----------------------------------------------------------------
   public String toString()
   {  
	  if (locked) return "locked";
      String faceName;
      if (face == HEADS)
    	  faceName = "Heads";
      else
    	  faceName = "Tails";
      
      return faceName;
   }
}