package problem2;
import java.util.*;

public class Driver {
	public static void main(String[] args)
	{

	    Coin myCoin = new Coin();
	    boolean ifLocked = myCoin.locked();

	    Scanner scan = new Scanner(System.in);
	    System.out.println("Please enter the password before you play: ");
	    int input= scan.nextInt();
	    myCoin.setKey(123);
        
	    if (input == myCoin.getKey())
	    {
	      System.out.println("Now you can play!");

	      myCoin.flip();

	      System.out.println(myCoin);
	       if (myCoin.isHeads())
	          System.out.println("You win.");
	       else
	          System.out.println("Better luck next time.");
        } 
	    else
	    {
	      System.out.println("Forget your password?");
	    }

	  }

}
