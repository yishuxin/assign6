//********************************************************************
//File:         Problem1.java       
//Author:       [Yishu Xin]
//Date:         [NOV, 8 2017]
//Course:       CPS100
//
//Problem Statement:
//Write a Java interface called Priority that includes two methods: setPriority
//and getPriority. The interface should define a way to establish numeric priority
//among a set of objects. Design and implement a class called Task that represents 
//a task (such as on a to-do list) that implements the Priority interface. Create 
//a driver class to exercise some Task objects.

//Inputs: none 
//Outputs: *shape
// 
//********************************************************************

public class Task implements Priority{
	
private String tasks;
private int priorityLevel;



public Task(String tasks, int priorityLevel )
{  this.tasks = tasks ;
   this.priorityLevel = priorityLevel;
 
   
}


public void setPriority(int priortyLevel)
{
	this.priorityLevel = priortyLevel;
	
}

public int getPriority()
{
	return priorityLevel;
}

public String toString()
{
return tasks +  ":"+ "\n" + "Priority Level: " + priorityLevel ;
}
	
}



